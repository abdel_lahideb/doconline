package com.formation.fitec.doc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    EditText nom;
    EditText prenom;
    EditText email;
    EditText age;
    EditText ta;
    EditText ta2;
    EditText fc;
    EditText sa;
    EditText glycemie;
    EditText renseignement;
    RadioGroup sexe;
    Button annuler;
    Button valider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nom = (EditText) findViewById(R.id.editTextNom);
        prenom = (EditText) findViewById(R.id.editTextPrenom);
        email = (EditText) findViewById(R.id.editTextEmail);
        age = (EditText) findViewById(R.id.editTextAge);
        ta = (EditText) findViewById(R.id.editTextTa);
        ta2 = (EditText) findViewById(R.id.editTextTa2);
        fc = (EditText) findViewById(R.id.editTextFC);
        sa = (EditText) findViewById(R.id.editTextSa);
        glycemie = (EditText) findViewById(R.id.editTextGlycemie);
        renseignement = (EditText) findViewById(R.id.editTextRc);
        annuler = (Button) findViewById(R.id.buttonAnnuler);
        valider = (Button) findViewById(R.id.buttonValider);
        sexe = (RadioGroup) findViewById(R.id.sexe);

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nom.setText("");
                prenom.setText("");
                email.setText("");
                age.setText("");
                ta.setText("");
                ta2.setText("");
                fc.setText("");
                sa.setText("");
                glycemie.setText("");
                renseignement.setText("");

            }
        });

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int idradio = sexe.getCheckedRadioButtonId();
                String sexeMessage = "";
                RadioButton sexeradio = (RadioButton) findViewById(idradio);
                if (sexeradio != null) {
                 sexeMessage  = sexeradio.getText().toString();
                }

                String nomMessage = nom.getText().toString();
                String prenomMessage = prenom.getText().toString();
                String emailMessage = email.getText().toString();
                String ages = age.getText().toString();
                String taMessage = ta.getText().toString();
                String ta2Message = ta2.getText().toString();
                String fcMessage = fc.getText().toString();
                String saMessage = sa.getText().toString();
                String glycemieMessage = glycemie.getText().toString();
                String renseignementMessage = renseignement.getText().toString();

                String stringTa = taMessage +" / "+ta2Message;

                //if (!sexeMessage.equals("") && !nomMessage.equals("") && !prenomMessage.equals("") && !ages.equals("") && !emailMessage.equals("") && !taMessage.equals("") && !fcMessage.equals("") && !saMessage.equals("") && !renseignementMessage.equals("")) {
                if (!nomMessage.equals("") && !prenomMessage.equals("")) {
                    int ageMessage = Integer.valueOf(ages);
                    Message message = new Message(sexeMessage, nomMessage, prenomMessage, emailMessage, stringTa,  fcMessage ,ageMessage, saMessage,glycemieMessage, renseignementMessage);

                    Intent email = new Intent(Intent.ACTION_SEND);
                    email.putExtra(Intent.EXTRA_EMAIL, new String[]{"docteurmandiogou@gmail.com"});
                    email.putExtra(Intent.EXTRA_SUBJECT, "urgences cardio");
                    email.putExtra(Intent.EXTRA_TEXT, message.toString());
                    email.setType("message/rfc822");
                    startActivity(Intent.createChooser(email, "Envoyez l'email avec l'application de votre choix :"));
                } else {
                    Toast.makeText(MainActivity.this, "Vous devez remplir obligatoirement le nom et le prénom", Toast.LENGTH_LONG).show();
                }


            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
