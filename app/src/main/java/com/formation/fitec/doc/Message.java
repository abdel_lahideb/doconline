package com.formation.fitec.doc;

import java.io.Serializable;

/**
 * Created by Fitec on 02/02/2015.
 */
public class Message implements Serializable{
    private String sexe;
    private String nom;
    private String prenom;
    private String email;
    private int age;
    private String ta;
    private String fc;
    private String sa;
    private String glycemie;
    private String renseignement;

    public Message(String sexe, String nom, String prenom, String email, String ta, String fc, int age, String sa, String glycemie, String renseignement) {
        this.sexe = sexe;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.ta = ta;
        this.fc = fc;
        this.age = age;
        this.sa = sa;
        this.glycemie = glycemie;
        this.renseignement = renseignement;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTa() {
        return ta;
    }

    public void setTa(String ta) {
        this.ta = ta;
    }

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public String getSa() {
        return sa;
    }

    public void setSa(String sa) {
        this.sa = sa;
    }

    public String getGlycemie() {
        return glycemie;
    }

    public void setGlycemie(String glycemie) {
        this.glycemie = glycemie;
    }

    public String getRenseignement() {
        return renseignement;
    }

    public void setRenseignement(String renseignement) {
        this.renseignement = renseignement;
    }

    @Override
    public String toString() {
        return "Message: " +
                "Sexe :" + sexe + "\n" +
                "Nom :" + nom + "\n" +
                "Prenom :" + prenom + "\n" +
                "Email :" + email + "\n" +
                "Age :" + age +"\n" +
                "ta :" + ta + "\n" +
                "fc :" + fc + " batt/min\n" +
                "sa :" + sa + " %\n" +
                "glycémie :" + glycemie + " g/l\n" +
                "renseignements cliniques :" + renseignement;
    }
}
